# Food SQL Database / React App

This is another simple React App example. Like the others, it's a React app
connected to an SQL database of food entries. A user enters a food name, and
it pulls all nutritional information for all entries that contain the name string.

It features a simple search bar, a basic CSS / HTML display to display the most 
important nutritional details, and some basic text wrapping.

This app works by running through pm2 / nginx on a cloud server.


