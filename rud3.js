const express = require('express');
const app = express();

var Pool = require('pg').Pool;

var config ={

	host: 'localhost',
	user: 'servertester',
	password: 'megaman3',
	database: 'food_nutrition',
};

var pool = new Pool(config);

app.set('port', process.env.PORT || 3001);

app.get('/api/food', async (req, res) => {

	console.log(req.query);
	var food = req.query.q;

	try{
	var response = await pool.query('select * from from entries where description like \'%$1%\'', [food]);
	}catch(e){
		console.log('Error running search', e);
	}

	res.json(JSON.stringify(response.rows));


});