import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';


var axios = require('axios');

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      results: []
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    var value = event.target.value;

    this.setState(function () {
      return {
        name: value
      }
    });

    if(this.state.name !== ''){
    let url = 'http://54.91.136.133/api/food?food=' + this.state.name;

    axios.get(url)
      .then(response => {
        let data = {
          results: response.data,
        };
        console.log(data);
        this.setState(data);

      })
      .catch(error => console.log(error));
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    let url = 'http://52.23.202.39/api/food?food=' + this.state.name;

    axios.get(url)
      .then(response => {
        let data = {
          results: response.data,
        };
        console.log(data);
        this.setState(data);

      })
      .catch(error => console.log(error));
  }


  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Food Nutrition</h1>
        </header>
        <p className="App-intro">
          Enter a food to research:
        </p>
   
      <form className='column' onSubmit={this.handleSubmit}>
        <label className='header' htmlFor='username'>{this.props.label}</label>
        <input
          id='name'
          placeholder='Food Name'
          type='text'
          value={this.state.name}
          autoComplete='off'
          onChange={this.handleChange}
        />
      </form>

          {this.state.results &&
            <table>
            <thead>
            <tr className ='row'>
            <td className ='Name'><h2>Name</h2></td>
            <td className ='Kcal'><h2>Kcal</h2></td>
            <td className ='Protein'><h2>Protein (g)</h2></td>
            <td className ='Fat'><h2>Fat (g)</h2></td>
            <td className ='Carbohydrate'><h2>Carb (g)</h2></td>


            </tr>
            </thead>
            

            {this.state.results.map((food) => {
                return(
                  <tr xy ='row'> 
                  <td Name = 'Name'> {food.description} </td>
                  <td Name = 'Kcal'> {food.kcal} </td>
                  <td Name = 'Protein'> {food.protein_g} </td>
                  <td Name = 'Fat'> {food.fa_sat_g} </td>
                  <td Name = 'Carbohydrate'> {food.carbohydrate_g} </td>
                  </tr>
                  )
            })
          }

          </table>
}
    </div>
    );
  }

}



export default App;